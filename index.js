const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')

app.use(bodyParser.json())

//allow for cross-origin resource sharing 
app.use(cors())

// implement token for morgan middleware
app.use(morgan(':method :url :res[content-length] = :response-time ms :body'))
morgan.token('body', function (req, res) {
	return JSON.stringify(req.body)
})

let persons = [{
		id: 1,
		name: "Bob the builder",
		number: "604-123-4523"
	},
	{
		id: 2,
		name: "Dora the explorer",
		number: "604-983-2343"
	},
	{
		id: 3,
		name: "Caillou",
		number: "604-111-2222"
	}
]


app.get('/info', (req, res) => {
	const length = persons.length
	const date = new Date();
	res.send('Phonebook has info for ' + length + ' people \n' + date)

})

app.get('/api/persons/:id', (req, res) => {
	const id = Number(req.params.id)
	//	console.log(id)
	const person = persons.find(person => person.id === id)
	//	console.log(person)
	if (person) {
		res.json(person)
	} else {
		res.status(404).end()
	}
})

app.delete('/api/persons/:id', (req, res) => {
	const id = Number(req.params.id)

	// filter out the id that we want to delete
	persons = persons.filter(person => person.id !== id)

	res.status(204).end()
})


const generateId = () => {
	const personId = persons.length > 0 ? Math.floor(Math.random() * 100) : 1
	return personId
}

app.post('/api/persons', (req, res) => {
	const body = req.body
	// console.log(body.name)

	if (!body.name || !body.number) {
		return res.status(400).json({
			error: 'either missing name and or number'
		})
	}
	let contact = JSON.stringify(persons.filter(person => person.name === body.name))
	// console.log(typeof(contact))
	if (contact !== "[]") {
		console.log("already exists")
		return res.status(400).json({
			error: 'this entry already exists in the phonebook'
		})
	}

	const person = {
		id: generateId(),
		name: body.name,
		number: body.number,
	}

	persons = persons.concat(person)

	// console.log(person)

	res.json(person)
})

const unknownEndpoint = (req, res) => {
	res.status(404).send({
		error: 'unknown endpoint'
	})
}

app.use(unknownEndpoint)

const PORT = process.env.PORT || 3001 
app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`)
})
