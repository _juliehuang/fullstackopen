import React from 'react';
import axios from 'axios';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: "" };
    }

    getInfo() {
        axios.get('http://localhost:3001/info').then(res => {
            console.log(res.data);
            this.setState({ data: res.data });
        })
    }

    componentWillMount() {
        this.getInfo();
    }

    render() {
        return <div className="App">
            <p className="App-intro">{this.state.data}</p>
        </div>



    }
}
export default App;
